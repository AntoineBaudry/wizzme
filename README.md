# ** WizzMe ** #

WizzMe est un projet réalisé en première année de stage.

## **Cas d'utilisation** ##

![casUtilisationWizzMe.png](https://bitbucket.org/repo/M9pBnn/images/1870842865-casUtilisationWizzMe.png)



### Contexte ###

WizzMe est une **application iOS** qui permet aux employés qui sont sur le terrain de pointer leurs **horaires**, et leur **position** depuis leur téléphone professionnel **sans avoir à passer par les locaux**.


### Outils et Logiciels utilisés ###

* Windev Mobile
* MacBook pour générer l'application
* iPhone de test


### Description ###

Une base de données existait déjà pour le pointage (cf Annexe 1), cependant j'ai du repérer quelles tables j'allais utiliser (cf Annexe 2).

J'ai du créer les fenêtres et les composants de toutes les fenêtres (cf Annexe 3).
Puis j'ai du créer des requêtes SQL (cf Annexe 4) pour : 

* récupérer les informations concernant un pointage 
* récupérer tous les pointages effectués par un employé à une date donnée (choisi par l'utilisateur sur le calendrier)
* donner la possibilité d'annuler un pointage sans le supprimer
* créer un pointage en récupérant les coordonnées gps (cf Annexe 5).

Ensuite j'ai du faire la présentation des fenêtres, pour une utilisation plus agréable.

Puis pour finir, j'ai effectué une série de test avec un utilisateur de test


### Annexes ###

**Annexe 1 : La base de données**


![annexe1.png](https://bitbucket.org/repo/M9pBnn/images/3983848294-annexe1.png)



**Annexe 2 : Table utilisée**


![annexe2.png](https://bitbucket.org/repo/M9pBnn/images/3678390919-annexe2.png)



**Annexe 3 : Les différentes fenêtres**


![annexe3.png](https://bitbucket.org/repo/M9pBnn/images/1164274453-annexe3.png)



**Annexe 4 : Les différentes requêtes**


![annexe4.png](https://bitbucket.org/repo/M9pBnn/images/2837363484-annexe4.png)



**Annexe 5 : Le code pour récupérer les coordonnées GPS**


![annexe5.png](https://bitbucket.org/repo/M9pBnn/images/2079966887-annexe5.png)